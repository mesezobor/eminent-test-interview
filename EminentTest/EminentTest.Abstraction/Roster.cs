﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EminentTest.Abstraction
{
    public class Roster
    {
        public int RosterId { get; set; }
        public string RosterName { get; set; }
        public string RosterColor { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
