import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  isActiveLink: boolean;
  public pageType : string;

  constructor(private _router: Router) {
    this.isActiveLink = true;
    this.pageType = 'HOME';
  }

  ngOnInit() {
  }

  onNavigateClick(questionType: string) {
    this.isActiveLink = true;
    this._router.navigate(['question/' + questionType]);
  }
}
