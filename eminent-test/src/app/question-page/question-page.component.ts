import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question-page',
  templateUrl: './question-page.component.html',
  styleUrls: ['./question-page.component.css']
})
export class QuestionPageComponent implements OnInit {
  questionType: any;
  pageType: string ;
  constructor(private route: ActivatedRoute, ) {
    this.questionType = this.route.snapshot.paramMap.get('questionType');
  }

  ngOnInit() {
  }

}
