import { Component, OnInit, Input } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() pageType: string;
  @Input() questionType: string;
  toggleHeaderText: boolean;
  constructor(private router: Router) {
  }

  ngOnInit() {
    if (this.pageType === 'HOME') {
      this.toggleHeaderText = true;
    } else {
      this.toggleHeaderText = false;
    }
  }
  onNavigateToHome() {
    this.router.navigateByUrl('/home');
  }

}
